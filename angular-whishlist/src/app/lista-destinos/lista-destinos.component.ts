import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';
import { DestinosApiClient } from '../models/destinos-api-client.models';
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onitemAdded: EventEmitter<DestinoViaje>;
  all;
  // destinos: DestinoViaje[];
  constructor(private destinosApiClient: DestinosApiClient) {
    // this.destinos = [];
    this.onitemAdded = new EventEmitter();
  }

  ngOnInit(): void {
  }

  /*guardar(n: string, u: string): boolean {
    this.destinos.push(new DestinoViaje(n, u));
    return false;
  }*/
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onitemAdded.emit(d);

  }

  /*elegido(d: DestinoViaje) {
    this.destinos.forEach(function (x) { x.setSelected(false); });
    d.setSelected(true);
  }*/

  elegido(e: DestinoViaje) {
    this.destinosApiClient.getAll().forEach(function (x) { x.setSelected(false); });
    e.setSelected(true);
  }
}
