import { Component, OnInit } from '@angular/core';
import { DestinoHotel } from './../models/destino-hotel.model';

@Component({
  selector: 'app-lista-hoteles',
  templateUrl: './lista-hoteles.component.html',
  styleUrls: ['./lista-hoteles.component.css']
})
export class ListaHotelesComponent implements OnInit {
  destinos: DestinoHotel[];
  constructor() { 
  	this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
  	this.destinos.push(new DestinoHotel (nombre, url));
  	return false;
  }

  hotelElegido(d : DestinoHotel){
    this.destinos.forEach(function (x) {
      x.setSelected(false);
    });
    d.setSelected(true);
  }
}
