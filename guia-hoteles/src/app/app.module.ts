import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { DestinoHotelComponent } from './destino-hotel/destino-hotel.component';
import { ListaHotelesComponent } from './lista-hoteles/lista-hoteles.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path:'home', component: ListaHotelesComponent},
    { path:'destino', component: DestinoDetalleComponent}
  ];
@NgModule({
  declarations: [
    AppComponent,
    DestinoHotelComponent,
    ListaHotelesComponent,
    DestinoDetalleComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
