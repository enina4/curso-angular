import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoHotelComponent } from './destino-hotel.component';

describe('DestinoHotelComponent', () => {
  let component: DestinoHotelComponent;
  let fixture: ComponentFixture<DestinoHotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoHotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoHotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
