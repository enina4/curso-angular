import { Component, OnInit, Input, HostBinding , EventEmitter, Output} from '@angular/core';
import { DestinoHotel } from './../models/destino-hotel.model';


@Component({
  selector: 'app-destino-hotel',
  templateUrl: './destino-hotel.component.html',
  styleUrls: ['./destino-hotel.component.css']
})
export class DestinoHotelComponent implements OnInit {
 @Input() destino: DestinoHotel;
 @Input('idx') position: number;
 @HostBinding('attr.class') cssClass = 'col-md-4';

 @Output() clicked: EventEmitter<DestinoHotel>;
  constructor() {
  	this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  elegir(): boolean{
  	this.clicked.emit(this.destino);
  	return false;
  }
}
